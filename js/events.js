function loadProductsByParent()
{
	var val = document.getElementById('parents').value;
	var url = '/loadProductsByParent';
	$.ajax({
		method: 'get',
		url: url,
		dataType: 'json',
		data:{val:val},
		success: function(resp){
			$('#products').empty();
			$.each(resp,function(key, value)
				{
					$('<option>').val(value['code_1c']).text(value['name']).appendTo('#products');
				});
			}
			});
}


function updateProductTable()
{
	var val = document.getElementById('command_id_1c').value;
	var url = '/loadProductsCommand';
	$.ajax({
		method: 'get',
		url: url,
		dataType: 'json',
		data:{val:val},
		success: function(resp){
			$('#content').remove();
			$.each(resp,function(key, value)
				{
					$('<option>').val(value['code_1c']).text(value['name']).appendTo('#content');
				});
			}
			});
}

function set_command_status(id_command,id_status,base_path){
	var url=base_path+'/save_command';
	$.ajax({
		method :'get',
		url    : url, 
		data   :{id_command:id_command,id_status:id_status},
		success:function(resp)
		{
			alert('Заявка сохранена успешно');
		},
		error  :function(xhr,ajaxOptions,thrownError)
		{
			if(xhr.status==200)
	        {

	        }	
	        else
	        {
	        	alert('Заявка НЕ СОХРАНЕНА. Проверь интернет подключения');
	        }
		}
	})
}


function updatePosition(pos,id_command)
{
	var url = 'updatePosition';
	$.ajax({
		method: 'get',
		url: url,
		data:{position:pos,id_command:id_command},
		success: function(resp){
			ShowDialogBox('Warning','Record updated successfully.','Ok','', 'GoToAssetList',null);
		},
		error: function (xhr, ajaxOptions, thrownError) {
	        if(xhr.status==200)
	        {

	        }	
	        else
	        {

	        }
	        
      	}
	});
}

function callClient(id_command,base_path)
{
	var url=base_path+'/callCreateEvent';
	$.ajax(
	{
		method: 'get',
		url:url,
		data:{id_command:id_command},
		success:function(resp)
		{
			alert('Событие отправленно оператору');
		},
		error:function(xhr,ajaxOptions,thrownError)
		{
			if(xhr.status==200)
	        {

	        }	
	        else
	        {
	        	alert('Ошибка отправки оператору. Проверь интернет подключения');
	        }
		}
	});
}

function saveToDatabase(editableObj) {
	//$(editableObj).css("background","#FFF url(loaderIcon.gif) no-repeat right");

	var url=$(editableObj).attr('base_path')+'/'+'save_quantity_return';
	$.ajax({
		url: url,
		type: "get",
		data:{column:'quantity_returnFact',id_doc:$(editableObj).attr('id_doc'),position:$(editableObj).attr('position'),product_code:$(editableObj).attr('product_code'),val:$(editableObj).text()},
		success: function(data){
			$(editableObj).css("background","blue");
		},
		error:function(xhr,ajaxOptions,thrownError)        
		{
			console.log(xhr);
		}
   });
}

$(function ($) {

	$('#parents').change(function()
	{
		loadProductsByParent();
	});
	 $('#commands').contextmenu({
        target: '#context-menu',
        scopes: 'tbody > tr',
        onItem: function (row, e) {
            var href = $(row).attr('href');
            var action = $(e.target).attr('id');
            var id_command=$(row).attr('id_command');
            var base_path=$(row).attr('base_path');
	        if(action=='open_command')
	        {
	           document.location=href;
	        }else if(action=='call')
	        {
	        	callClient(id_command,base_path);
	        }
            
        }
    });

	$('.btn_delete_product').click(function(e){
		e.preventDefault();
		url=$(this).attr('url');
	    $.ajax({
			method:'get',
			url:url,
			success:function(resp)
			{
				//console.log(resp);
			//	$('#content tbody').remove();
				$('#content>tbody').html(resp);
			},
			error:function(xhr,ajaxOptions,thrownError){
			}

	    });
	});
	$('.pos').click(function(e)
		{
			e.preventDefault();
			href=$(this).attr('href');
			$.ajax({
				method: 'get',
				url: href,
				dataType: 'text',
				success: function(resp){
					$(e.target).text(resp);
				}
			});
		}
	);
	var fixHelperModified = function(e, tr) {
	    var $originals = tr.children();
	    var $helper = tr.clone();
	    $helper.children().each(function(index) {
	        $(this).width($originals.eq(index).width())
	    });
	    return $helper;
	},
    updateIndex = function(e, ui) {
        $('td.index', ui.item.parent()).each(function (i) {
            $(this).html(i + 1);
            id=$(this)[0]['attributes']['id_command'].value;
            pos=$(this).html();
            updatePosition(pos,id);
        });

    };
	$("#commands tbody").sortable({
		scroll: true,
	   	helper: fixHelperModified,
	    stop: updateIndex
	}).disableSelection();
	$( "#commands tbody" ).sortable( "disable" );

	$('#btn_route_modify').click(function(e) {
		if($("#btn_route_modify").text()=="Начать редактирование МЛ")
		{
            $("#btn_route_modify").text("Закончить редактирование МЛ");
            $( "#commands tbody" ).sortable( "enable" );
		}
		else 
		{
			$("#btn_route_modify").text("Начать редактирование МЛ");
			$( "#commands tbody" ).sortable( "disable" );
		}
	});

});

function moveRow()
{
	$(".up,.down").click(function(){
        var row = $(this).parents("tr:first");
        var par=row.prev();
        if ($(this).is(".up")) {
            row.insertBefore(row.prev().prev().prev());
            par.insertBefore(par.prev().prev().prev());
        } else {
            row.insertAfter(row.next().next());
            par.insertAfter(par.next().next());
        }
    });
}
$(document).ready(function(){
	var offset = 100,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
	offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
	scroll_top_duration = 700,
		//grab the "back to top" link
	$back_to_top = $('.cd-top');

	//hide or show the "back to top" linkc
	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if( $(this).scrollTop() > offset_opacity ) { 
			$back_to_top.addClass('cd-fade-out');
		}
	});
	$("#dialog").dialog({
	      autoOpen: false,
	      modal: true
	    });
	//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		 	}, scroll_top_duration
		);
	});
	moveRow();
	//
	loadProductsByParent();		
});
function show_m()
{
	value_box=$('#quantity_box').val();
	//$('#quantity_box').val(0);
}
function getIndex(x) {
   // alert("Row index is: " + x.rowIndex);
}

