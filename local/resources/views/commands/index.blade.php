@extends('layouts.app')

@section('content')
<div class="container">
      <div class="row">
        <div id="context-menu">
          <ul class="dropdown-menu" role="menu">
             <li><a tabindex="-1" id="open_command"><span class="glyphicon glyphicon-eye-open"></span>Открыть Заявку</a></li>
             <li><a tabindex="-1" id="call"><span class="glyphicon glyphicon-earphone"></span>Позвонить</a></li>
             <li><a tabindex="-1" id="free">Something else here</a></li>
          </ul>
       </div>
        <div class="btn-group btn-breadcrumb">
            <a href="{!!Url('/home')!!}" class="btn btn-info">Маршруты</a>
        </div>
        <a class="btn btn-info" id="btn_route_modify" style="position: fixed;z-index: 101;">Начать редактирование МЛ</a>
          <div class="panel panel-primary filterable">
              <div class="panel-heading">
                  <h3 class="panel-title">Зявки</h3>
                  <div class="pull-right">
                      <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filtru</button>
                  </div>
              </div>
            <table class="table table-striped table-bordered comm_in_route is-scrollable" id="commands" style="border-collapse:collapse;">
              <thead>
                  <tr class="filters">
                      <th width="5%"><input type="text" class="form-control" placeholder="№" disabled></th>
                      <th><input type="text" class="form-control" placeholder="Контрагент" disabled></th>
                      <th><input type="text" class="form-control" placeholder="Адрес" disabled></th>
                    </tr>
                </thead>
              <tbody>
                <?php $i=0;?>
                @foreach($commands as $command)
                  <tr data-toggle="collapse" class="strikeout" base_path="{!!URL::to('/');!!}" id_command="{!!$command->id!!}" data-target="#demo<?php echo $i;?>" style="{!!$command->checked==1 ? 'background-color:#c5e3c4' : ''!!}" class="accordion-toggle" href="view_command/{!!$route_id!!}/{!!$command->id!!}">
                    <td class="index" id_command="{!!$command->id!!}" width="5%">{!!$command->position!!}</td>
                    <td>{!!$command->doc_problems->count()>0 ? '<span class="glyphicon glyphicon-warning-sign text-danger"></span>' : ''!!}{!!$command->counteragent!!}
                    </td>
                    <td>{!!$command->address!!} <span class="label label-success" >{!!$command->schedule!!}</span></td>                    
                  </tr>
                  <!---->
                  <?php $i++;?>
                @endforeach
              </tbody>
            </table>
      </div>

    </div>
    <a href="#0" class="cd-top">Top</a>
  </div>

@endsection
