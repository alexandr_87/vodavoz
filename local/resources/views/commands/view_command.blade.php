@extends('layouts.app')

@section('content')
<div class="container">

  <div class="btn-group btn-breadcrumb">
      <a href="{!!Url('/home')!!}" class="btn btn-info">Маршруты</a>
      <a href="{!!Url('open_route/'.$route_id)!!}" class="btn btn-info">Заявки</a>
  </div>
      <div class="row">
        <div class="col-md-12">
           <div class="panel with-nav-tabs panel-info">
               <div class="panel-heading">
                       <ul class="nav nav-tabs">
                           <li class="active"><a href="#tab1info" data-toggle="tab">Проблемные чеки</a></li>
                           <li><a href="#tab2info" data-toggle="tab">Заявка</a></li>
                       </ul>
               </div>
               <div class="panel-body">
                   <div class="tab-content">
                       <div class="tab-pane fade in active" id="tab1info">Проблемные чеки

                         <table id="content" class="table" id="commands" style="border-collapse:collapse;">
                           <thead>
                             <tr>
                               <th>Доп Документ</th>
                               <th>Инфо. о доставке</th>
                             </tr>
                           </thead>
                           @foreach($c->doc_problems as $dp)
                             <tr>
                               <td>{!!$dp->box!!}</td>
                               <td>{!!$dp->info!!}</td>
                             </tr>
                           @endforeach
                         </table>

                       </div>
                       <div class="tab-pane fade" id="tab2info">
                         <div class="col-xs-6">
                             {!! Form::label('', 'Номер заявки') !!}
                             {!! Form::text('docnom', $c->code,['class'=>'form-control','disabled']) !!}
                         </div>
                         <div class="col-xs-6">
                             {!! Form::label('', 'Дата заявки') !!}
                             {!! Form::text('docdata', $c->data,['class'=>'form-control','disabled']) !!}
                         </div>
                         <div class="col-xs-6">
                             {!! Form::label('', 'Клиент') !!}
                             {!! Form::text('counteragent', $c->counteragent,['class'=>'form-control','disabled']) !!}
                         </div>
                         <div class="col-xs-6">
                             {!! Form::label('', 'Адрес') !!}
                             {!! Form::text('address', $c->address,['class'=>'form-control','disabled']) !!}
                         </div>
                         <div class="col-xs-6">
                             {!! Form::label('', 'Ответственный') !!}
                             {!! Form::text('person', $c->person,['class'=>'form-control','disabled']) !!}
                         </div>
                         <div class="col-xs-6">
                             {!! Form::label('', 'График Работы') !!}
                             {!! Form::text('person', $c->schedule,['class'=>'form-control','disabled']) !!}
                         </div>
                         <div class="col-xs-6">
                           {!! Form::label('pGroup', 'Группа товара') !!}
                           <select name="parents" class="form-control" id="parents">
                             @foreach($parents as $parent)
                               <option value="{!!$parent->code_1c!!}">{!!$parent->name!!}</option>
                             @endforeach
                           </select>
                         </div>
                         <div class="col-xs-6">
                           {!! Form::label('pGroup', 'Товар') !!}
                           <select name="products" class="form-control" id="products">

                           </select>
                         </div>
                         <div class="col-xs-12">
                             <a href='#' id='btn_open' class= 'form-control btn btn-info' data-toggle="modal" data-target="#quantity"><span class="glyphicon glyphicon-plus"></span></a>
                         </div>
                         <br><br>
                         <div class="modal fade" id="quantity" role="dialog">
                             <div class="modal-dialog modal-sm">
                               <div class="modal-content">
                                 <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                                   <h4 class="modal-title">Введите количество товара</h4>
                                 </div>
                                 <div class="modal-body">
                                   <input type="number" id="quantity_box"/>
                                 </div>
                                 <div class="modal-footer">

                                 </div>
                               </div>
                             </div>
                         </div>
                        <!-- <input type="submit" class="btn btn-default" data-dismiss="modal" onclick="show_m()" id="btn_quantity"></input>-->
                           <table id="content" class="table" style="border-collapse:collapse;">
                             <thead>
                               <tr>
                                 <th>Товар</th>
                                 <th>Кол</th>
                                 <th>Цена</th>
                                 <th>Сумма</th>
                                 <th>На возврат</th>
                                 <th>Возврат по фак.</th>
                                 <th>X</th>
                               </tr>
                             </thead>
                             <tbody>
                               @foreach($command_content as $cc)
                                 <tr>
                                   <td>{!!$cc->productContent->name!!}</td>
                                   <td>{!!$cc->quantity_doc!!}</td>
                                   <td>{!!$cc->price!!}</td>
                                   <td>{!!$cc->price*$cc->quantity_doc!!}</td>
                                   <td>{!!$cc->quantity_return!!}</td>
                                   <td contenteditable="{!!$c->checked==4 ? 'false' : 'true'!!}" onBlur="saveToDatabase(this)" quantity_returnFac='quantity_returnFac' id_doc='{!!$cc->id_doc!!}' position='{!!$cc->position!!}' product_code='{!!$cc->product_code!!}' base_path='{!!URL::to('/')!!}'>
                                     {!!$cc->quantity_returnFact!!}
                                   </td>
                                   <td><a href='#' url='{!!URL::to("/")!!}/delete_product/{!!$cc->id_doc!!}/{!!$cc->position!!}/{!!$cc->product_code!!}' class= 'btn btn-danger btn-xs btn_delete_product'><span class="glyphicon glyphicon-remove"></span></a></td>
                                 </tr>
                               @endforeach
                             </tbody>
                           </table>
                          
                             <div class="col-xs-12">
                                <button type="button" id='btn_open' class= 'form-control btn btn-danger btn-xs' value="Согласен" onclick="set_command_status('{!!$c->id!!}',1,'{!!URL::to('/');!!}')">Согласен</button>
                             </div>
  

                        </div>
                   </div>
               </div>
           </div>
       </div>
      </div>
    </div>
@endsection
