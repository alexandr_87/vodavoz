@extends('layouts.app')

@section('content')
<div class="container">
      <div class="row">
          <div class="panel panel-primary filterable">
              <div class="panel-heading">
                  <h3 class="panel-title">Маршруты</h3>
                  <div class="pull-right">
                      <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filtru</button>
                  </div>
              </div>
        <table class="table table-condensed" id="commands" style="border-collapse:collapse;">
          <thead>
              <tr class="filters">
                  <th><input type="text" class="form-control" placeholder="#" disabled></th>
                  <th><input type="text" class="form-control" placeholder="№ Маршрута" disabled></th>
                  <th><input type="text" class="form-control" placeholder="Дата" disabled></th>
                  <th><input type="text" class="form-control" placeholder="Описание" disabled></th>
                  <th><input type="text" class="form-control" placeholder="Начать" disabled></th>
                  <th><input type="text" class="form-control" placeholder="Окончен" disabled></th>
                </tr>
            </thead>
          <tbody>
            @foreach($routes as $route)
                <?php $color=""; $button1="";$button2="";?>
                @if($route->status==1)
                  <?php $color="#c6e2ff"; $button1="disabled";?>
                @elseif ($route->status==2)
                    <?php $color="#eeeeee"; $button1="disabled"; $button2="disabled";?>
                @endif
                <tr bgcolor="{!!$color!!}" onclick="document.location = 'open_route/{!!$route->code_1c!!}';">
                  <td>{!!$route->id!!}</td>
                  <td>{!!$route->code!!}</td>
                  <td>{!!date("d-m-Y",strtotime($route->date))!!}</td>
                  <td>{!!$route->description!!}</td>
                <td><a href='#' id='btn_open' class= 'btn btn-danger btn-xs' {!!$button1!!}><span class="glyphicon glyphicon-ok"></span></a></td>
                <td><a href='#' id='btn_close' class= 'btn btn-danger btn-xs' {!!$button2!!}><span class="glyphicon glyphicon-remove"></span></a></td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </DIV>
        <div class="col-md-12">
            {!! $routes->links() !!}
        </div>
      </div>
</div>
@endsection
