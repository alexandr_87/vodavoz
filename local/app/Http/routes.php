<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routesu
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more. delete_product/{!!$cc->id_doc!!}/{!!$cc->position!!}/{!!$cc->product_code!!}'
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/', 'DocRoutesController@index');
    Route::get('/home', 'DocRoutesController@index');
    Route::get('/loadProductsByParent','CommandsController@loadProductsByParent');
    Route::get('/loadProductsCommand','CommandsController@loadProductsCommand');
    Route::get('open_route/updatePosition','CommandsController@updatePosition');
    Route::get('open_route/set_position/{command_id}/{route_id}','CommandsController@set_position');
    Route::get('open_route/{route_id}','CommandsController@index');
    Route::get('open_route/view_command/{route_id}/{command_id}','CommandsController@view');
    Route::get('save_command','CommandsController@save');
    Route::get('callCreateEvent','CommandsController@callCreateEvent');
    Route::get('delete_product/{id_doc}/{position}/{product_code}','CommandsController@delete_product');
    Route::get('save_quantity_return','CommandsController@save_quantity_return');
});
