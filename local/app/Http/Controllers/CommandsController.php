<?php

namespace App\Http\Controllers;
use App\CommandHeader;
use App\CommandContent;
use App\Product;
use App\ParentProduct;
use App\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;

class CommandsController extends Controller
{
 
    public function index(Request $request)
    {
      if(Auth::check())
      {
        $route_id=$request->route_id;
        $commands=CommandHeader::getHeader()->where('id_route','=',$route_id)->get();
        return view('commands.index',compact('route_id','commands'));
      }
      else {
        return view('auth.login');
      }
    }


    public function view(Request $request)
    {
      if(Auth::check())
      {
        $command=CommandHeader::getHeader()->where('id','=',$request->command_id);
        $route_id=$request->route_id;
        $c=$command->first();
        $parents=ParentProduct::select('id','code_1c','name')->get();
        $products=Product::select('id','code_1c','name','parent_id')->get();
        $command_content=$c->commandcontent;
        return view('commands.view_command',compact('route_id','products','parents','c','command_content'));
      }
      else {
        return view('auth.login');
      }
    }

    function set_position(Request $request)
    {
      $maxpos=CommandHeader::select('position')->where('id_route','=',$request->route_id)->max('position');
      $rez=CommandHeader::where('id','=',$request->command_id)->update([
        'position'=>(int)$maxpos+1
      ]);
      if(is_null($rez))
      {
        return 'ER';
      }else
      {
        return $maxpos+1;
      }
    }

     function updatePosition(Request $request)
    {
      $rez=CommandHeader::where('id','=',$request->id_command)->update([
        'position'=>(int)$request->position
      ]);
      if(is_null($rez))
      {
        return 'ER';
      }else
      {
        return 'OK';
      }
    }


    public function loadProductsByParent()
    {

        $products=Product::select('id','code_1c','name','parent_id')->where('parent_id','=',Input::get('val'))->get();
        return $products->toJson();
    }

    public function loadProductsCommand()
    {
      $command=CommandHeader::getHeader()->where('id','=',Input::get('val'));
      $c=$command->first();
      $command_content=$c->commandcontent;
      //$products=CommandContent::select()->where("")->get();
    }

    public function createEvent($id_command,$event)
    {
      Event::create([
          'id_doc'  =>$id_command,
          'event_id'=>$event,
          'user_id' =>Auth::user()->id
      ]);
      CommandHeader::where('id','=',$id_command)->update(
        [
          'checked'=>$event
        ]);
    }

    public function delete_product(Request $request)
    {
      $position=$request->position;
      $id_doc=$request->id_doc;
      $product_code=$request->product_code;
      $com=CommandContent::where('id_doc','=',$id_doc)->where('position','=',$position)->where('product_code','=',$product_code);
      $rez=$com->delete();
      $tbody=CommandContent::Where('id_doc','=',$id_doc)->get();
      $html=''; 
      foreach ($tbody as $cc) {
        $html=$html
        .'<tr>'
          .'<td>'.$cc->productContent->name.'</td>'
          .'<td>'.$cc->quantity_doc.'</td>'
          .'<td>'.$cc->price.'</td>'
          .'<td>'.$cc->price*$cc->quantity_doc.'</td>'
          .'<td>'.$cc->quantity_return.'</td>'
          .'<td contenteditable='.($cc->checked==4 ? 'false' : 'true').'>'.$cc->quantity_returnFact.'</td>'
          .'<td><a href="#" url="'.base_path().'/delete_product/'.$cc->id_doc.'/'.$cc->position.'/'.$cc->product_code.'" class= "btn btn-danger btn-xs btn_delete_product"><span class="glyphicon glyphicon-remove"></span></a></td>'
        .'</tr>';
      }
      return $html;
    }

    public function save()
    {
      $id_command=Input::get('id_command');
      $status=Input::get('id_status');
      $rez=CommandHeader::where('id','=',$id_command)->update([
        'checked'=>$status,
      ]);
      if(is_null($rez))
      {
        return 'err';
      }else
      {
        $this->createEvent($id_command,Event::DELIVERED);
      }
      return $id_command;
    }

    public function callCreateEvent()
    {
      $id_command=Input::get('id_command');
      $this->createEvent($id_command,Event::CALL);
    }
    public function save_quantity_return(Request $request)
    {
      $position=$request->position;
      $id_doc=$request->id_doc;
      $product_code=$request->product_code;
      CommandContent::where('id_doc','=',$id_doc)->where('position','=',$position)->where('product_code','=',$product_code)->update([
        'quantity_returnFact'=>$request->val,
      ]);
    }
}
