<?php

namespace App\Http\Controllers;

use App\DocRoute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;

class DocRoutesController extends Controller
{
    public function index()
    {
      if(Auth::check())
      {
        $routes=DocRoute::select('id','code','date','status','code_1c','description','user_id')->where('user_id','=',Auth::user()->id)->paginate(10);
        return view('routes.index',compact('routes'));
      }
      else {
        return view('auth.login');
      }

    }
}
