<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParentProduct extends Model
{
  protected $table="product_groups";
  protected $fillable = array('id','code_1c','name');

  function product()
  {
    return $this->hasMany("App\Product","parent_id","code_1c");
  }

}
