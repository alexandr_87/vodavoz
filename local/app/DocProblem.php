<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocProblem extends Model
{
    protected $table="problems";
    protected $fillable = array('id','id_doc','client','box','id_doc','quantity_returnFact','price');
    public function commandheader()
    {
      return $this->belongsTo("App\CommandHeader");
    }

}
