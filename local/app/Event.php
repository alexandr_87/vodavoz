<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
	const DELIVERED=4; 
	const CALL=6;
	const CALLED=7;
	const FAIL_DELIVERED=5;
    protected $table="events";
    protected $fillable=array('id_doc','event_id','user_id');
}
