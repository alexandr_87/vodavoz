<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocRoute extends Model
{
    protected $table="routes";
    protected $fillable = array('id','code','date','status','code_1c','description');

    public function commands()
    {
      return $this->hasMany('App\CommandContent','id_route','code_1c');
    }
}
