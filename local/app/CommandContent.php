<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommandContent extends Model
{
    protected $table="commands_content";
    protected $fillable = array('id','id_doc','product_code','quantity_doc','quantity_return','quantity_returnFact','price');
    public function commandheader()
    {
      return $this->belongsTo("App\CommandHeader");
    }

    public function productContent()
    {
      return $this->hasOne("App\Product","code_1c","product_code");
    }
}
