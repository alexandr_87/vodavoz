<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommandHeader extends Model
{
    protected $table="commands";
    protected $fillable = array('id','code_1c','data','counteragent','address','person','payment_type','checked','id_route','code','position');
    public function commandcontent()
    {
      return $this->hasMany("App\CommandContent","id_doc","code_1c");//foreign key; local key
    }
    public function route()
    {
      return $this->belongsTo('App\DocRoute');
    }

    public static function getHeader()
    {
      return CommandHeader::select('id','code','code_1c','counteragent','address','payment_type','data','person','schedule','position','checked')->orderBy('position', 'asc');
    }

    function doc_problems()
    {
      return $this->hasMany('App\DocProblem','id_doc','code_1c');
    }

}
