<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  protected $table="products";
  protected $fillable = array('id','code_1c','name','articul','parent_id');

  function commandcontent()
  {
    return $this->belongsTo('App\CommandContent');
  }

  function parent()
  {
    return $this->belongsTo("App\ParentProduct");
  }
}
